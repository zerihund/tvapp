
import Foundation
class TV {
     let channelCount :Int
     var volume:Int?
     var channel:Int?
     var on = false
    
    init(channelCount:Int) {
        self.channelCount = channelCount
    }
    
    func pressOnOff(){
        on = true
        channel = 1
        volume = 5
    }
    func volumeUp(){
        if volume ?? 5 < 10{
            volume? += 1
        }
        
    }
    func volumeDown()  {
        if(volume ?? 5 > 0){
            volume? -= 1
        }
        
        
    }
    func channelUp() {
        if(channel ?? 1 < 5){
        channel? += 1
        }
    }
    func channelDown(){
        if(channel ?? 1 > 1){
            channel? -= 1
        }
        else{
            channel? = channelCount
        }
    }
}
